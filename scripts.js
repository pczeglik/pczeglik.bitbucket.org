'use strict';

var APP = (function(window, document, undefined) {

	var CSS_CLASS = {
		ACTIVE: 'active',
		STICKY: 'sticky',
		COMPACT: 'compact'
	};

	function bindEvents(config) {
		var section = document.querySelector('section');
		section.addEventListener('click', config.onSectionClick);

		window.addEventListener('scroll', config.onWindowScroll);
	}

	function onSectionClick(event) {
		var validNodes = ['hgroup', 'h1', 'h2'];
		var targetNodeName = event.target.nodeName.toLowerCase();
		var parent = null;

		if (validNodes.indexOf(targetNodeName) > -1) {
			switch (targetNodeName) {
				case 'hgroup':
					parent = event.target.parentElement;
					break;

				case 'h1':
				case 'h2':
					parent = event.target.parentElement.parentElement;
					break;
			}

			if (parent) {
				triggerClick(parent);
			}
		}
	}

	function triggerClick(parent) {
		var activeElement = document.querySelector('.' + CSS_CLASS.ACTIVE);
		
		if (parent.classList.contains(CSS_CLASS.ACTIVE)) {
			collapseElement(parent);
			return;
		}

		toggleActiveElement(activeElement, parent);
	}

	function toggleActiveElement(activeElement, newElement) {
		if (activeElement) {
			collapseElement(activeElement);
		}

		if (newElement) {
			expandElement(newElement);
		}
	}

	function collapseElement(el) {
		var hgroup = el.querySelector('hgroup');

		el.classList.remove(CSS_CLASS.ACTIVE);

		if (hgroup) {
			hgroup.classList.remove(CSS_CLASS.STICKY);
			hgroup.classList.remove(CSS_CLASS.COMPACT);
		}
	}

	function expandElement(el) {
		el.classList.add(CSS_CLASS.ACTIVE);
	}

	function onWindowScroll() {
		var activeElement = document.querySelector('.' + CSS_CLASS.ACTIVE);
		var scrollY = window.scrollY;
		var deltaY = 0;
		var hgroup = null;

		if (activeElement) {
			deltaY = activeElement.offsetTop - scrollY;
			hgroup = activeElement.querySelector('hgroup');

			if (deltaY < 0) {
				hgroup.classList.add(CSS_CLASS.STICKY);

				if (Math.abs(deltaY) > hgroup.offsetHeight / 4) {
					hgroup.classList.add(CSS_CLASS.COMPACT);
				} else {
					hgroup.classList.remove(CSS_CLASS.COMPACT);
				}
			} else {
				hgroup.classList.remove(CSS_CLASS.STICKY);
				hgroup.classList.remove(CSS_CLASS.COMPACT);
			}
		}
	}

	var API = {
		start: function() {
			bindEvents({
				onSectionClick: onSectionClick,
				onWindowScroll: onWindowScroll
			});
		}
	};

	return API;
}(this, this.document));

APP.start();
